== Comment fonctionne une imprimante 3D ?
image::images/buse.jpg[Buse d'imprimante 3D en action, 300, 300]
La buse de l'imprimante va se déplacer sur 3 axes (X, Y, Z) en tridimensionnelle et va fabriquer une pièces par ajout de matière en l'occuranse du plastique en général du PLA.

video::videos/impression3D.webm[height=200]
